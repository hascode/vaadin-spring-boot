package com.hascode.tutorial.view;

import com.hascode.tutorial.Customer;
import com.hascode.tutorial.CustomerRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;

@Route("grid")
public class SampleGridView extends VerticalLayout {

  final Grid<Customer> grid;

  public SampleGridView() {
    this.grid = new Grid<>();

Customer customer = new Customer("Tim", "Fisher");


    grid.setSizeFull();
    grid.addColumn(Customer::getFirstName);
    grid.addColumn(Customer::getLastName);

    grid.setItems(customer);
    add(new Label("Grid Example"), grid);
  }

}

